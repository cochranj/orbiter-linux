// Edits to add buffer size:
// Copyright (c) 2023, Jack Cochran
// Licensed under the MIT License

#pragma once
//#include "glad.h"

#include <stdint.h>

class VertexBuffer {
public:
    VertexBuffer(const void *data, uint32_t size);
    ~VertexBuffer();

    void Bind() const;
    void UnBind() const;

    void *Map() const;
    void UnMap() const;

    void Update(const void *data, int size);

    const uint32_t Size() const {
        return numBytes;
    }

private:
    GLuint VBID;
    uint32_t numBytes;
};

class IndexBuffer {
    using index_type = uint16_t;
public:
    IndexBuffer(index_type* indices, uint32_t count);
    ~IndexBuffer();

    void Bind() const;
    void UnBind() const;
    void *Map() const;
    void UnMap() const;
    uint32_t GetCount() const { return count; }
private:
    GLuint IBID;
    uint32_t count;
};

class VertexArray {
public:
    VertexArray();
    ~VertexArray();

    void Bind() const;
    void UnBind() const;
    IndexBuffer *GetIBO() const { return IBO; }
private:
    GLuint VAID;
    IndexBuffer *IBO;
    VertexBuffer *VBO;
};
