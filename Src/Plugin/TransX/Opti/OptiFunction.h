/* Opti/OptiFunction.h -- from enjomitch-orbiter-addons
 *
 * Copyright © 2007-9 Steve Arch, Duncan Sharpe
 * Copyright © 2011 atomicdryad - 'ENT' button & Pen allocation fix
 * Copyright © 2013 Dimitris Gatsoulis (dgatsoulis) - Hacks
 * Copyright © 2013 Szymon Ender (Enjo) - Auto-Min™, Auto-Center™ &
 *     other hacks
 *
 * X11 License ("MIT License")
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#ifndef OPTIFUNCTION_H
#define OPTIFUNCTION_H

//#include <Math/BinSearchOptiSubject.hpp>
#include <Math/Opti/OptiSubject.hpp>
#include <Math/Opti/OptiMultiSubject.hpp>
#include "VarConstraint.h"
class basefunction;
class Intercept;

// Base for the "Optimisation Problem". Could be used for multiple dimension optimisation
class OptiFunctionBase
{
    public:
        OptiFunctionBase(basefunction * base, Intercept * icept)
           : m_base(base)
           , m_icept(icept)
        {}
    protected:
        double RecalculateGetValue();
        basefunction * m_base;

    private:

        Intercept * m_icept;
};

// Defines the one dimensional "Optimisation Problem"
class OptiFunction : public OptiFunctionBase, public EnjoLib::OptiSubject
{
    public:
        OptiFunction(VarConstraint toOpti, basefunction * base, Intercept * icept)
        : OptiFunctionBase(base, icept)
        , m_toOpti(toOpti)
        {
        }
        double UpdateGetValue( double arg );
    private:
        VarConstraint m_toOpti;
};

class OptiMultiFunction : public OptiFunctionBase, public EnjoLib::OptiMultiSubject
{
    public:
        OptiMultiFunction(std::vector<VarConstraint> toOpti, basefunction * base, Intercept * icept)
        : OptiFunctionBase(base, icept)
        , m_toOpti(toOpti)
        {
        }
        double Get(const double * in, int n);

        std::vector<double> GetStart() const;
        std::vector<double> GetStep() const;

    private:
        std::vector<VarConstraint> m_toOpti;
};

#endif // OPTIFUNCTION_H
