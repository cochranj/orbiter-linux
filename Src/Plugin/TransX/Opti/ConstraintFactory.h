/* Opti/ConstraintFactory.h -- from enjomitch-orbiter-addons
 *
 * Copyright © 2007-9 Steve Arch, Duncan Sharpe
 * Copyright © 2011 atomicdryad - 'ENT' button & Pen allocation fix
 * Copyright © 2013 Dimitris Gatsoulis (dgatsoulis) - Hacks
 * Copyright © 2013 Szymon Ender (Enjo) - Auto-Min™, Auto-Center™ &
 *     other hacks
 *
 * X11 License ("MIT License")
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef CONSTRAINTFACTORY_H
#define CONSTRAINTFACTORY_H

#include "ConstraintType.h"

class Constraint;
class basefunction;
class ConstraintFactory
{
    public:
        ConstraintFactory(basefunction * base);
        virtual ~ConstraintFactory();
        Constraint Create(ConstraintType::e type);


    protected:
    private:
        Constraint CreateProgradeHohmann(basefunction * base);
        Constraint CreateProgradeManoeuvre(basefunction * base);
        Constraint CreateOutward();
        Constraint CreatePlane();
        Constraint CreateAngle();

        basefunction * m_base;
};

#endif // CONSTRAINTFACTORY_H
