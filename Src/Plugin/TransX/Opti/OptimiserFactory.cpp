/* Opti/OptimiserFactory.cpp -- from enjomitch-orbiter-addons
 *
 * Copyright © 2007-9 Steve Arch, Duncan Sharpe
 * Copyright © 2011 atomicdryad - 'ENT' button & Pen allocation fix
 * Copyright © 2013 Dimitris Gatsoulis (dgatsoulis) - Hacks
 * Copyright © 2013 Szymon Ender (Enjo) - Auto-Min™, Auto-Center™ &
 *     other hacks
 * Copyright © 2023 Jack Cochran - Switch to std::unique_ptr
 *
 * X11 License ("MIT License")
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#include "OptimiserFactory.h"
#include "Optimiser.h"

#include <memory>

OptimiserFactory::OptimiserFactory(basefunction * base, Intercept * icept)
: m_base(base)
, m_icept(icept)
{
}

OptimiserFactory::~OptimiserFactory()
{
}

std::unique_ptr<Optimiser> OptimiserFactory::Create(const std::vector<VarConstraint> & pArgs2Find)
{
    return std::make_unique<Optimiser>(m_base, m_icept, pArgs2Find);
}
std::unique_ptr<Optimiser> OptimiserFactory::Create(VarConstraint pArg2Find)
{
    return std::make_unique<Optimiser>(m_base, m_icept, pArg2Find);
}
std::unique_ptr<Optimiser> OptimiserFactory::CreateDummy()
{
    //return std::auto_ptr<Optimiser>(Create(std::vector<MFDvarfloat*>()));
    return nullptr;
}
