/* Opti/ConstraintFactory.cpp -- from enjomitch-orbiter-addons
 *
 * Copyright © 2007-9 Steve Arch, Duncan Sharpe
 * Copyright © 2011 atomicdryad - 'ENT' button & Pen allocation fix
 * Copyright © 2013 Dimitris Gatsoulis (dgatsoulis) - Hacks
 * Copyright © 2013 Szymon Ender (Enjo) - Auto-Min™, Auto-Center™ &
 *     other hacks
 *
 * X11 License ("MIT License")
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#include "ConstraintFactory.h"
#include "Constraint.h"
#include "../basefunction.h"
#include <Math/Constants.hpp>

static const double precisionVel = 0.001;
//static const double precisionVel = 0.00001;
static const double precisionAngle = 0.000001;
//static const double precisionAngle = 0.000001;

ConstraintFactory::ConstraintFactory(basefunction * base)
: m_base(base)
{
}

ConstraintFactory::~ConstraintFactory()
{
}

Constraint ConstraintFactory::Create(ConstraintType::e type)
{
    switch (type)
    {
    case ConstraintType::PROGRADE_HOHMANN:
        return CreateProgradeHohmann(m_base);
    case ConstraintType::PROGRADE_MANOEUVRE:
        return CreateProgradeManoeuvre(m_base);
    case ConstraintType::CHANGE_PLANE:
        return CreatePlane();
    case ConstraintType::OUTWARD:
        return CreateOutward();
    case ConstraintType::ANGLE:
        return CreateAngle();
    }
    return Constraint(0, 0, precisionVel);
}

Constraint ConstraintFactory::CreateOutward()
{
    // Outward velocity requires a lot of energy. Keep it small
    const double lower = -3000;
    const double upper = +3000;
    return Constraint(lower, upper, precisionVel);
}

Constraint ConstraintFactory::CreatePlane()
{
    const double lower = -4.5e3;
    const double upper = +4.5e3;
    return Constraint(lower, upper, precisionVel);
}

Constraint ConstraintFactory::CreateAngle()
{
    const double lower = -180 * RAD;
    const double upper = +180 * RAD;
    return Constraint(lower, upper, precisionAngle);
}

Constraint ConstraintFactory::CreateProgradeHohmann(basefunction * base)
{
    const double defaultRatioHohmann = 0.15;
    const double hohmanDV = base->GetHohmannDV();
    const double lower = hohmanDV * (1 - defaultRatioHohmann);
    const double upper = hohmanDV * (1 + defaultRatioHohmann);

    return Constraint(lower, upper, precisionVel);
}

// Doesn't work very well
Constraint ConstraintFactory::CreateProgradeManoeuvre(basefunction * base)
{
    const double defaultRatioHohmann = 0.05;
    const double hohmanDV = base->GetHohmannDV();
    const double lower = 0;//hohmanDV * (1 - defaultRatioHohmann);
    const double upper = hohmanDV * (1 + defaultRatioHohmann);
    return Constraint(lower, upper, precisionVel);
}

