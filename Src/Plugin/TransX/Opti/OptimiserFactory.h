/* Opti/OptimiserFactory.h -- from enjomitch-orbiter-addons
 *
 * Copyright © 2007-9 Steve Arch, Duncan Sharpe
 * Copyright © 2011 atomicdryad - 'ENT' button & Pen allocation fix
 * Copyright © 2013 Dimitris Gatsoulis (dgatsoulis) - Hacks
 * Copyright © 2013 Szymon Ender (Enjo) - Auto-Min™, Auto-Center™ &
 *     other hacks
 * Copyright © 2023 Jack Cochran - Switch to std::unique_ptr
 *
 * X11 License ("MIT License")
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef OPTIMISERFACTORY_H
#define OPTIMISERFACTORY_H

#include "VarConstraint.h"
#include <memory>
#include <vector>

class basefunction;
class Intercept;
class Optimiser;

/// Creates optimizers
/**
Should be created on stack, and not stored, due to storing pointers
*/
class OptimiserFactory
{
    public:
        OptimiserFactory(basefunction * base, Intercept * icept);
        virtual ~OptimiserFactory();

        std::unique_ptr<Optimiser> Create(const std::vector<VarConstraint> & pArgs2Find);
        std::unique_ptr<Optimiser> Create(VarConstraint pArg2Find);
        std::unique_ptr<Optimiser> CreateDummy();

    protected:
    private:
        basefunction * m_base;
        Intercept * m_icept;
};

#endif // OPTIMISERFACTORY_H
