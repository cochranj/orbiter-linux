# CMakeLists.txt
#
# libMFD--previously known as "EnjoLibMFD" from a separate repository.
#
# Copyright (c) 2023, Jack Cochran
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.



add_library(MFD SHARED
    MFDButtonPage.cpp

    # Not used by TransX and has a dependency on HDC from
    # Windows.h and Orbiter 2006.
    #MFDHUDDrawer/IMFDDrawsHUD.cpp
    #MFDHUDDrawer/MFDHUDDrawer.cpp

    # Candidate to merge with some unified sound framework.
    # Disabled because depends on Win32 sound API.
    #MFDSound++/Sound.cpp
    #MFDSound++/SoundMap.cpp

    Orbiter/AngularAcc.cpp
    Orbiter/AutopilotRotation.cpp
    Orbiter/BurnTime.cpp
    Orbiter/Engine.cpp
    Orbiter/SketchpadDummy.cpp
    # Disabled due to dependency on libkost, which is LGPL. Not used by
    # TransX.
    #Orbiter/SpaceMathKOST.cpp
    Orbiter/SpaceMathOrbiter.cpp
    Orbiter/SystemsConverterOrbiter.cpp
    Orbiter/UtilOrbiter.cpp
    Orbiter/VesselCapabilities.cpp

    # Disabled due to dependency on Utils/CharManipulations.hpp from
    # enjomitchs /lib folder.  Only used by MFDSound++.
    #Utils/FileUtilsOrbiter.cpp  # Candidate to merge with Orbitersdk file
                                 # utils
    Utils/MFDButtonPage.cpp
    # Disabled due to apparent dependency on Win32 GDI functions.
    # These are form a self-contained unit--unused.
    #Utils/MFDTextCalculator.cpp
    #Utils/MyDC.cpp
    #Utils/Pens.cpp
)

target_include_directories(MFD
    PUBLIC ${ORBITER_SOURCE_SDK_INCLUDE_DIR}
    PUBLIC ${MODULE_COMMON_DIR}/libMFD
    PUBLIC ${MODULE_COMMON_DIR}/EnjoLib
)

target_link_libraries(MFD
    ${ORBITER_SDK_LIB}
    EnjoLib
)

add_dependencies(MFD
    ${OrbiterTgt}
    Orbitersdk
    EnjoLib
)

# Installation
install(TARGETS MFD
    # Runtime
    DESTINATION ${ORBITER_INSTALL_PLUGIN_DIR}
)
# No docs
