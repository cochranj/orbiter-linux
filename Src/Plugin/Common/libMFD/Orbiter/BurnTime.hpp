/* Orbiter/BurnTime.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.
 * The implementation source file contains the following statement.
 *
 * Copyright (c) 2007 Steve Arch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF, OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef BURNTIME_H
#define BURNTIME_H

#include <Orbitersdk.h>
#include <vector>

class BurnTime
{
    public:
        BurnTime();
        virtual ~BurnTime();

        double GetBurnStart(VESSEL *vessel, THGROUP_TYPE thGroupType, double instantaneousBurnTime, double deltaV);
        double GetBurnTimeVariadic(VESSEL *vessel, THGROUP_TYPE thGroupType, double deltaV);
        double GetBurnTime(VESSEL *vessel, double deltaV);
        double GetStackMass(VESSEL *vessel);
        void AddVesselToStack(VESSEL *vessel, std::vector<VESSEL*> &stack);
    protected:
    private:
};

#endif // BURNTIME_H
