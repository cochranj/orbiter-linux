/* Orbiter/SketchpadDummy.cpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Related code may be LGPL 3, BSD, or
 * MIT licensed.
 */

#include "SketchpadDummy.h"

using namespace oapi;
using namespace EnjoLib;

SketchpadDummy::SketchpadDummy()
: Sketchpad(NULL)
{
}

SketchpadDummy::~SketchpadDummy()
{
}
