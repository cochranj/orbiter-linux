/* Orbiter/SpaceMathOrbiter.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.
 * The implementaiton source file contains the following statement.
 *
 * +Modified BSD License^M
 *
 * This file is a part of Math package and originates from:
 * http://sf.net/projects/enjomitchsorbit
 *
 * Copyright (c) 2012, Szymon "Enjo" Ender
 * Copyright (c) 2008, Steve "agentgonzo" Arch - GetHeadBearing()
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the <organization> nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SPACEMATHORBITER_H
#define SPACEMATHORBITER_H

#include <Orbitersdk.h>
namespace EnjoLib
{
class SpaceMathOrbiter
{
    public:
        double GetPlaneAngle( const VESSEL * v, const OBJHANDLE hTarget ) const;
        double GetHeadBearing( const VESSEL * v ) const;
        void Crt2Pol (VECTOR3 &pos, VECTOR3 &vel) const;
        void Crt2Pol (VECTOR3 &pos) const;
        double getdeterminant(const MATRIX3 & mat) const;
        MATRIX3 getinvmatrix(const MATRIX3 & mat) const;
        VECTOR3 ToEquatorial( const VECTOR3 & in, const OBJHANDLE hRef ) const;
        VECTOR3 GetPlaneAxis( const OBJHANDLE hObj, const OBJHANDLE hRef ) const;
        void getinvrotmatrix(VECTOR3 arot, MATRIX3 *invrotmatrix) const;//arot not really a vector - see arot defn from vessel struct
        VECTOR3 GetRotationToTarget(VESSEL * vessel, const VECTOR3 & target) const;
        /// Swaps coordinate system
        /** Orbiter uses left hand coordinates because of DirectX legacy. */
        VECTOR3 SwapCoordinateSystem( const VECTOR3 & in ) const;
    protected:
    private:
};
}
#endif // SPACEMATHORBITER_H
