/* Orbiter/Engine.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Related code may be LGPL 3, BSD, or
 * MIT licensed.
 */

#ifndef ENGINE_H
#define ENGINE_H

struct Engine
{
    Engine()
    {
        F = isp = ThrAngle = 0;
    }
    double F, isp, ThrAngle;
};

#endif // ENGINE_H
