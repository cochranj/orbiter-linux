/* Orbiter/SketchpadDummy.h -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Related code may be LGPL 3, BSD, or
 * MIT licensed.
 */

#ifndef SKETCHPADDUMMY_H
#define SKETCHPADDUMMY_H

#include <Orbitersdk.h>

namespace EnjoLib
{
class SketchpadDummy : public oapi::Sketchpad
{
    public:
        SketchpadDummy();
        virtual ~SketchpadDummy();

        oapi::Font *SetFont (oapi::Font *font) const { return NULL; }
        oapi::Pen *SetPen (oapi::Pen *pen) const { return NULL; }
        oapi::Brush *SetBrush (oapi::Brush *brush) const { return NULL; }
        void SetTextAlign (TAlign_horizontal tah=LEFT, TAlign_vertical tav=TOP) {}
        int SetTextColor (int col) { return 0; }
        int SetBackgroundColor (int col) { return 0; }
        void SetBackgroundMode (BkgMode mode) {}
        int GetCharSize () { return 0; }
        int GetTextWidth (const char *str, int len = 0) { return 0; }
        void SetOrigin (int x, int y) {}
        bool Text (int x, int y, const char *str, int len) { return false; }
        bool TextBox (int x1, int y1, int x2, int y2, const char *str, int len) { return false; }
        void Pixel (int x, int y, int col) {}
        void MoveTo (int x, int y) {}
        void LineTo (int x, int y) {}
        void Line (int x0, int y0, int x1, int y1) {}
        void Rectangle (int x0, int y0, int x1, int y1) {}
        void Ellipse (int x0, int y0, int x1, int y1) {}
        void Polygon (const oapi::IVECTOR2 *pt, int npt) {}
        void Polyline (const oapi::IVECTOR2 *pt, int npt) {}
        void PolyPolygon (const oapi::IVECTOR2 *pt, const int *npt, const int nline) {}
        void PolyPolyline (const oapi::IVECTOR2 *pt, const int *npt, const int nline) {}
    protected:
    private:
};
}
#endif // SKETCHPADDUMMY_H

