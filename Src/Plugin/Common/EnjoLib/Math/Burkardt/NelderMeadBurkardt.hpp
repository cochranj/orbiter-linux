/* Burkardt/NelderMeadBurkardt.hpp -- from enjomitch-orbiter-addons
 * No original attribution statement.
 * The implementation source file contains the following licensing statement.
 *
 * nelmin()
 * Licensing:
 *
 *   This code is distribtued under the GNU LGPL license.
 *
 * Modified:
 *
 *   27 February 2008
 *
 * Author:
 *
 *   Original FORTRAN77 version by R ONeill.
 *   C++ version by John Burkardt.
 *
 * timestamp()
 * Licensing:
 *
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *
 *   24 September 2003
 *
 * Author:
 *
 *   John Burkardt
 */

namespace EnjoLib{
class OptiMultiSubject;
}

namespace NelderMeadBurkardt
{
    void nelmin ( EnjoLib::OptiMultiSubject & subj, int n, double start[], double xmin[],
      double *ynewlo, double reqmin, const double step[], int konvge, int kcount,
      int *icount, int *numres, int *ifault );
    void timestamp ( );
}
