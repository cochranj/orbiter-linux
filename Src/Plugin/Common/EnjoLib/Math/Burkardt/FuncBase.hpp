/* Burkardt/FuncBase.hpp -- from enjomitch-orbiter-addons
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef FUNCBASE_H
#define FUNCBASE_H

namespace EnjoLib {

class FuncBase
{
    public:
        FuncBase();
        virtual ~FuncBase();

        virtual double operator() (double) = 0;
    protected:
    private:
};
}
#endif // FUNCBASE_H
