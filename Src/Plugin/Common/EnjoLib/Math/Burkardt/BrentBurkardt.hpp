/* Burkardt/BrentBurkardt.hpp -- from enjomitch-orbiter-addons
 * No original attribution statement.
 * The implementation source file contains the following attribution:
 *
 * Licensing:
 *
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *
 *   17 July 2011
 *
 * Author:
 *
 *   Original FORTRAN77 version by Richard Brent.
 *   C++ version by John Burkardt. https://people.sc.fsu.edu/~jburkardt/cpp_src/brent/brent.html
 *   Further C++ modifications by Szymon Ender "Enjo". www.enderspace.de
 */

#ifndef BRENTBURKARDT_H
#define BRENTBURKARDT_H

#include <vector>
#include "../../Util/Result.hpp"

namespace EnjoLib {

class FuncBase;
class OptiSubject;

class BrentBurkardt
{
    public:
        double glomin ( double a, double b, double c, double m, double e, double t,
          FuncBase & f, double &x );
        Result<double> local_min ( double a, double b, double t, int maxIter, OptiSubject & f,
          double &x );
        double local_min_rc ( double &a, double &b, int &status, double value );
        double r8_abs ( double x );
        double r8_epsilon ( );
        double r8_max ( double x, double y );
        double r8_sign ( double x );
        void timestamp ( );
        double zero ( double a, double b, double t, FuncBase & f );
        void zero_rc ( double a, double b, double t, double &arg, int &status,
          double value );

        // === simple wrapper functions
        // === for convenience and/or compatibility
        double glomin ( double a, double b, double c, double m, double e, double t,
          double f ( double x ), double &x );
        double local_min ( double a, double b, double t, double f ( double x ),
          double &x );
        double zero ( double a, double b, double t, double f ( double x ) );
};

}

#endif // BRENTBURKARDT_H
