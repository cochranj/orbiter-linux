/* MaxMinFindF.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef MAXMINFINDF_H
#define MAXMINFINDF_H

#include "MaxMinFind.hpp"

namespace EnjoLib
{
class MaxMinFindF : public MaxMinFind<float>
{
    public:
        MaxMinFindF() {}
        virtual ~MaxMinFindF() {}

    protected:

    private:
};
}

#endif // MAXMINFINDF_H
