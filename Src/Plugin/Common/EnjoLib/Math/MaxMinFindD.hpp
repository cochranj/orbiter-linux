/* MaxMinFindD.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef MAXMINFINDD_HPP
#define MAXMINFINDD_HPP

#include "MaxMinFind.hpp"

namespace EnjoLib
{
class MaxMinFindD : public MaxMinFind<double>
{
    public:
        MaxMinFindD() {}
        virtual ~MaxMinFindD() {}

    protected:

    private:
};
}

#endif // MAXMINFINDD_HPP
