/* Root/RootType.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef ROOTTYPE_H
#define ROOTTYPE_H

namespace EnjoLib
{
    enum RootType
    {
        ROOT_BIN_SEARCH,
        ROOT_BRENT
    };
}

#endif // ROOTTYPE_H
