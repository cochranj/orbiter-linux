/* SolverQuartic.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef SOLVERQUARTIC_H
#define SOLVERQUARTIC_H

#include <complex>
#include <array>

class SolverQuartic
{
    public:
        SolverQuartic();
        virtual ~SolverQuartic();

        typedef std::array<std::complex<double>, 4> Roots;
        Roots Run(double a, double b, double c, double d, double e);

    protected:
    private:
};

#endif // SOLVERQUARTIC_H
