/* Opti/OptiType.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef OPTITYPE_H
#define OPTITYPE_H

namespace EnjoLib
{
    enum OptiType
    {
        OPTI_BIN_SEARCH,
        OPTI_BRENT
    };
}

#endif // OPTITYPE_H
