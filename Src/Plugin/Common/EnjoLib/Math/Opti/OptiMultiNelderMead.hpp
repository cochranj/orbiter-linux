/* Opti/OptiMultiNelderMead.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 */

#ifndef OPTIMULTINELDERMEAD_H
#define OPTIMULTINELDERMEAD_H

#include <vector>
#include "../../Util/Result.hpp"

namespace EnjoLib
{
    class OptiMultiSubject;

    class OptiMultiNelderMead
    {
        public:
            OptiMultiNelderMead();
            virtual ~OptiMultiNelderMead();

            Result<std::vector<double> > Run( OptiMultiSubject & subj, double eps = 0.00001, int konvge = 10, int kcount = 500 ) const;
        protected:
        private:
    };
}

#endif // OPTIMULTINELDERMEAD_H
