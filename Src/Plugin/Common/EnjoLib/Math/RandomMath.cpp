/* RandomMath.cpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  Some code in this folder is BSD
 * licensed, while other code is LGPL.
 *
 * Edits to use ctime header:
 * Copyright (c) 2023, Jack Cochran
 * Licensed under the MIT license
 */

#include "RandomMath.hpp"
#include <random>
#include <ctime>

using namespace EnjoLib;

RandomMath::RandomMath()
{
    //ctor
}

RandomMath::~RandomMath()
{
    //dtor
}


void RandomMath::RandSeed(int seed) const
{
    if (seed == 0)
        srand (std::time(NULL));
    else
        srand (seed);
}

double RandomMath::Rand(double min, double max) const
{
    return rand() / static_cast<double>(RAND_MAX) * (max-min) + min;
}
