/* Statistical/VectorTpl.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.  The implementation source file
 * contains the following statement.
 *
 * Modified BSD License
 *
 * This file is a part of Statistical package and originates from:
 * http://sf.net/projects/enjomitchsorbit
 *
 * Copyright (c) 2012, Szymon "Enjo" Ender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER>
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef VECTORTPL_H
#define VECTORTPL_H


#include <vector>
#include <string>
namespace EnjoLib
{
template<class T>
class VectorTpl : public std::vector<T>
{
    public:
        std::string Print() const; // TODO
        std::string PrintScilab( const char * varName ) const;
        std::string PrintPython( const char * varName ) const;

        VectorTpl( const std::vector<T> & init );
        VectorTpl( const std::vector<bool> & init );
        VectorTpl( int n );
        VectorTpl();
        virtual ~VectorTpl();

        void Add(const T & val);
        void Add(const VectorTpl<T> & vec);

        //! Length of vector
        T Len() const;
        //! Normalised copy of vector
        VectorTpl Norm() const;
        T SumSquares() const;
        T SumAbs() const;
        T Sum() const;
        T Mean() const;
        T MeanWeighted() const;
        T Max() const;
        T Min() const;
        VectorTpl AdjustMean() const;
        VectorTpl Slice  (unsigned idx, unsigned len) const;
        VectorTpl SliceTS(unsigned idx, unsigned len) const;
        VectorTpl Diffs() const;

        VectorTpl & operator += (const VectorTpl & par);
        VectorTpl & operator -= (const VectorTpl & par);
        VectorTpl & operator /= (const T f);
        VectorTpl & operator *= (const T f);
        VectorTpl & operator += (const T f);
        VectorTpl & operator -= (const T f);

        VectorTpl operator + (const VectorTpl & par) const;
        VectorTpl operator - (const VectorTpl & par) const;
        VectorTpl operator - () const;
        VectorTpl operator + (const T f) const;
        VectorTpl operator - (const T f) const;
        VectorTpl operator * (const T f) const;
        VectorTpl operator / (const T f) const;
        bool operator > (const VectorTpl & par) const;
        bool operator < (const VectorTpl & par) const;



    protected:
    private:
        void SizesEqual( const VectorTpl & par, const char * functionName ) const;

        typedef typename std::vector<T>::const_iterator CIt;
        typedef typename std::vector<T>::iterator It;
};
}

#endif // VECTORTPL_H
