/* Statistical/3rdParty/EigenMKL.hpp -- from enjomitch-orbiter-addons.
 * No original attribution statement.
 */

#ifndef EIGENMKL_HPP
#define EIGENMKL_HPP

#include "../EigenAbstract.hpp"

namespace EnjoLib
{
class EigenMKL : public EigenAbstract
{
    public:
        EigenMKL();
        virtual ~EigenMKL();

    protected:
        std::vector<EigenValueVector> GetEigenValVecClient( const Matrix & m ) const;

    private:
};
}

#endif // EIGENMKL_HPP
